// import {gulp} from 'gulp'
// import {shell} from 'gulp-shell'

const gulp = require('gulp');
const shell = require('gulp-shell')

// const files = ['*.ts']
const files = ['mapper/maps/*.json']

gulp.task('build', shell.task('tsc'))

gulp.task('test', shell.task('jest'))

gulp.task(
  'coveralls',
  gulp.series('test', shell.task('coveralls < ./coverage/lcov.info'))
)

gulp.task('lint', shell.task('eslint ' + files.join(' ')))

gulp.task('format', shell.task('yarn prettier --write ' + files.join(' ')))

//gulp.task('default', gulp.series('build', 'test', 'lint', 'format'))
gulp.task('default', gulp.series('format'))

gulp.task(
  'watch',
  gulp.series('default', () => {
    gulp.watch(files, gulp.task('default'))
    // gulp.watch('fixture/report_test/map_report.js', gulp.task('default'))
  })
)
// const { src, dest,watch, series } = require('gulp');
// const shell = require('gulp-shell')
// // var jsonFormat = require('gulp-json-format');
// //const prettierJson = require('gulp-prettier');
// //const { objectMap } = require('mingo/util');
// // const map = require('map-stream');
// // const { ObjectMapper } = require('./dist');
// // const JSON_Report = require('./fixture/pi4_xml.json');
// // const jsonfile = require('jsonfile')
// const include = require('gulp-include');


// exports.default = function () {
//   watch('mapper/out/*.json', validate)
// }

// exports.mapper = function() {
//   watch('mapper/maps/*.json',{ ignoreInitial: false }, prettier)
//   //  {
//   //   src('mapper/maps/*.json')
//   //     .pipe(include())
//   //     .on('data',function(newData){
//   //       shell.task('yarn prettier --write ' + newData.path);
//   //     })
//   //   cb()
//   // })
// }

// function prettier(cb) {
//   shell.task('echo hello')
//   //shell.task('yarn prettier --write /Volumes/DeeDee/repositories/calibrate-json-mapper/mapper/maps/mapped_001.json')
//   // src('mapper/maps/*.json')
//   //   .pipe(jsonFormat(2))
//   //   .pipe(dest('.'))
//   cb()
// }

// // function validate(cb){
// //   src('mapper/out/*.json')
// //     .pipe((prettier())
// //   dest('')
// // }

// // function mapper(cb){
// //   src('mapper/maps/*.js')
// //     .pipe(include())
// //       .on('error',console.log)
// //       .on('data',function(newData){
        
// //         console.log(String(newFile.contents))
// //       })

// //       // .on('success',function(){
// //       //   console.log('success');
// //       //   console.log(arguments.length);
// //       // })
// //     // .pipe(map(function(file,cb){
// //     //   const mapper = require(file.path).mapper
// //     //   console.log(mapper.version)
// //     //   //const fileMap = require(file.path)
// //     //  //console.log(fileMap.mapper.version)
// //     //   // if(file.)
// //     //   //var fileContents = JSON.parse(file.contents.toString())
// //     //   //console.log(fileContents)
// //     //   // var mapped = ObjectMapper(JSON_Report, fileMap.map);
// //     //   // console.log(JSON.stringify(mapped))
// //     //   // file.contents = Buffer.from(JSON.stringify(mapped));
// //     //   cb(null,file)
// //     // }))
// //     //.pipe(dest('mapper/out'))
// //     cb()
// // }


// // // const mapTask = require('./fixture/report_test/map_task')


// // function prettier(cb) {
// //   shell.task('yarn prettier --write mapper/out/mapped_out.json');
// //   cb()
// // }

// // function javascript(cb) {
// //   // body omitted
// //   cb();
// // }

// // function css(cb) {
// //   // body omitted
// //   cb();
// // }

// // function mapTask(cb){
// //   return watch('mapper/map/*.js',(obj)=>{
// //     gulp.src(ObjectMapper)
// //   })
// //   ObjectMapper()
// // }

// // exports.mapOutWatch = function(){
// //   watch('mapper/out/*.json',(obj)=>{
// //     gulp.src(obj)
// //     .pipe()
// //   });
// // }

// // exports.default = function() {
// //   // You can use a single task
// //   watch('mapper/maps/*.js', series(mapTask));
// // };

// // //
// // var filter = require('gulp-filter');

// // function om(file){
// //   fs.readFile(file)
// //   ObjectMapper(JSON_Report,file)
// // }

// // function isAdded(file) {
// //     return file.event === 'add';
// // }

// // var filterAdded = filter(isAdded);

// // gulp.task('default', function () {
// //     return gulp.src('mapper/map/*.js')
// //         .pipe(watch('mapper/map/*.js'))
// //         .pipe(filterAdded)
// //         .pipe(gulp.dest('newfiles'))
// //         .pipe(filterAdded.restore())
// //         .pipe(gulp.dest('oldfiles'));
// // });

