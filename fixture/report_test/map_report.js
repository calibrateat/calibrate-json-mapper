const { ObjectMapper, getKeyValue } = require('../../dist/index');
const mingo = require('mingo');
const JSON_Report = require('../pi4_xml.json');
const fs = require('fs');
//-------

//-------

const map = {
  //'report.results[0].hits[].__attributes':'result.checks'

  'report.results[0].hits[].__attributes': [
    {
      key: 'checks',
      transform: function(found, src) {
        // found hits > build query
        let ruls = found.map(foundCheck => {
          return { '__attributes.id': foundCheck.rule_id };
        });
        let query = { $or: ruls };
        // position of checks
        let listOfChecksKey = 'report.profile_info[0].rules[0].rule';
        // filtered listOfChecks
        let listOfChecks = getKeyValue(src, listOfChecksKey);

        //
        // query = {
        //   $or: [
        //     { '__attributes.id': 'RUL219' },
        //     { '__attributes.id': 'RUL116' },
        //     { '__attributes.id': 'RUL111' },
        //     { '__attributes.id': 'RUL92' },
        //     { '__attributes.id': 'RUL89' },
        //     { '__attributes.id': 'RUL40' },
        //     { '__attributes.id': 'RUL30' },
        //     { '__attributes.id': 'RUL1' },
        //   ],
        // };

        // filter listOfChecks
        let filteredListOfChecks = mingo.find(listOfChecks, query).all();

        let checksMap = {
          '[].__attributes.id':'[].id',
          '[].display_name[0]':'[].name',
          '[].display_comment[0]':'[].description',
          '[].__attributes.custom_id':'[].customId',
          '[].rulesets[0].ruleset[0].severity[0]':'[].severity'
        }

        let mappedChecks = ObjectMapper(filteredListOfChecks,checksMap)

        // const checks = found.map(foundCheck=>{
        //   return {
        //     id:foundCheck.rule_id,
        //     serverity:  foundCheck.serverity
        //   }
        // })

        return mappedChecks;
      },
      default: null,
    },
    // {
    //   key: 'result.manFiltered',
    //   transform: function(found, src) {
    //     const query = {
    //       $or: [
    //         { '__attributes.id': 'RUL219' },
    //         { '__attributes.id': 'RUL116' },
    //       ],
    //     };
    //     const key = 'report.profile_info[0].rules[0].rule';
    //     let searchElm = getKeyValue(src, key);
    //     let checks = mingo.find(searchElm, query).all();
    //     return checks;
    //   },
    //   default: null,
    // },
  ],

  // // 'report.results[0]':{
  // //     key:'result.checks',
  // //     transform:function(value, src){
  // //         //let foundChecks = ObjectMapper(value.hits,)
  // //         // const query = {
  // //         //     "__attributes.id":"RUL1"
  // //         // }
  // //         const query = {
  // //             $or:[
  // //                 {"__attributes.id":"RUL1"},
  // //                 {"__attributes.id":"RUL219"},
  // //             ]
  // //         }
  // //         const key = "report.profile_info[0].rules[0].rule"
  // //         let searchElm = getKeyValue(src, key)
  // //         let checks = mingo.find(searchElm,query).all()
  // //         let count = mingo.find(searchElm,query).count()

  // //         return value
  // //         // return count
  // //         // return checks
  // //         // return 1
  // //     }
  // // }
};

/*
[{"rule_id":"RUL219","severity":"Warning"},{"rule_id":"RUL116","severity":"Error"},{"rule_id":"RUL111","severity":"Warning"},{"rule_id":"RUL92","severity":"Warning"},{"rule_id":"RUL89","severity":"Error"},{"rule_id":"RUL40","severity":"Error"},{"rule_id":"RUL30","severity":"Error"},{"rule_id":"RUL1","severity":"Error"}]
 */



  const mapped = ObjectMapper(JSON_Report, map);

  fs.writeFile(
    '/Volumes/DeeDee/repositories/calibrate-json-mapper/mapper/maps/mapped_001.json',
    JSON.stringify(mapped),
    {
      encoding: 'utf8',
      flag: 'w',
      mode: 0o666,
    },
    writeError => {
      if (writeError) throw writeError;
    }
  )


