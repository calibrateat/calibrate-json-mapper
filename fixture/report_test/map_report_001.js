const { ObjectMapper, getKeyValue } = require('../../dist/index');
const mingo = require('mingo');
const JSON_Report = require('../pi4_xml.json');
const fs = require('fs');
//-------

//-------

const map = {
  // info
  noMime: { key: 'info.mime', default: 'application/pdf' },
  'report.document[0].doc_info[0].filename': 'info.filename',
  'report.document[0].doc_info[0].path': 'info.filepath',
  'report.document[0].doc_info[0].pdfversion': 'info.version',
  'report.document[0].doc_info[0].created': 'info.created',
  'report.document[0].doc_info[0].modified': 'infomodified',
  //plates
  'report.document[0].doc_info[0].platenames[0].platename': {
    key: 'info.plates',
    transform: function(value) {
      let mapped = value.map(plate => {
        return {
          name: plate,
          type:
            plate === 'Cyan' ||
            plate === 'Magenta' ||
            plate === 'Yellow' ||
            plate === 'Black'
              ? 'process'
              : 'spot',
        };
      });
      return mapped;
    },
  },
  // pdfxInfo
  'report.document[0].doc_info[0].pdfxconformance':'info.pdfxInfo.pdfxVersion',
  'report.document[0].doc_info[0].trapped':'info.pdfxInfo.trapped',
  'report.document[0].resources[0].output_intents[0].output_intent[0].icc_profile[0].profile_description':'info.pdfxInfo.pdfxOutputintent.outputProfilename',
  'report.document[0].resources[0].output_intents[0].output_intent[0].output_profilename':'info.pdfxInfo.pdfxOutputintent.outputProfilename',
  'report.document[0].resources[0].output_intents[0].output_intent[0].icc_profile[0].colorspace_of_data':'info.pdfxInfo.pdfxOutputintent.colorspace',
  // results
  'report.results[0]': [
    {
      key: 'result.checks',
      transform: function(found, src) {
        // found hits > build query
        let ruls = found.hits.map(foundCheck => {
          return { '__attributes.id': foundCheck.__attributes.rule_id };
        });
        let query = { $or: ruls };
        // position of checks
        let listOfChecksKey = 'report.profile_info[0].rules[0].rule';
        // filtered listOfChecks
        let listOfChecks = getKeyValue(src, listOfChecksKey);
        // filter listOfChecks
        let filteredListOfChecks = mingo.find(listOfChecks, query).all();
        // checks map
        let checksMap = {
          '[].__attributes.id': '[].id',
          '[].display_name[0]': '[].name',
          '[].display_comment[0]': '[].description',
          '[].__attributes.custom_id': '[].customId',
          '[].rulesets[0].ruleset[0].severity[0]': '[].severity',
        };
        // map checks
        let mappedChecks = ObjectMapper(filteredListOfChecks, checksMap);
        //return
        return mappedChecks;
      },
      default: null,
    },
    {
      key: 'result.fixups',
      transform: function(found, src) {
        // found hits > build query
        let foundFixUp = found.fixup.map(foundFix => {
          return { '__attributes.fixup_id': foundFix.__attributes.fixup_id };
        });
        let query = { $or: foundFixUp };
        // // position of checks
        let listOfFixupsKey = 'report.profile_info[0].fixups[0].fixup';
        // filtered listOfChecks
        let listOfFixups = getKeyValue(src, listOfFixupsKey);
        // filter listOfChecks
        let filteredListOfFixups = mingo.find(listOfFixups, query).all();
        // fixups map
        let fixupsMap = {
          '[].__attributes.fixup_id': '[].id',
          '[].display_name[0]': '[].name',
          '[].display_comment[0]': '[].description',
          '[].__attributes.custom_id': '[].customId',
        };
        // map fixups
        let mappedFixups = ObjectMapper(filteredListOfFixups, fixupsMap);
        //return
        return mappedFixups;
      },
      default: null,
    },
  ],
};

const mapped = ObjectMapper(JSON_Report, map);
// console.log(JSON.stringify(mapped))

fs.writeFile(
  '/Volumes/DeeDee/repositories/calibrate-json-mapper/mapper/maps/mapped_001.json',
  JSON.stringify(mapped),
  {
    encoding: 'utf8',
    flag: 'w',
    mode: 0o666,
  },
  writeError => {
    if (writeError) throw writeError;
  }
);
