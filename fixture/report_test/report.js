map = {
    //'report.results[0].hits[].__attributes':'result.checks'
  
    'report.results[0].hits[].__attributes' : [
      {
        key: 'result.checksQuery',
        transform: function(found, src) {
          // found hits > build query
          let ruls = found.map(foundCheck => {
            return { '__attributes.id': foundCheck.rule_id };
          });
          let query = { $or: ruls };
          //
          let listOfChecksKey = 'report.profile_info[0].rules[0].rule';
          // filtered listOfChecks
          let listOfChecks = getKeyValue(src, listOfChecksKey);
  
          //
          // query = {
          //   $or: [
          //     { '__attributes.id': 'RUL219' },
          //     { '__attributes.id': 'RUL116' },
          //     { '__attributes.id': 'RUL111' },
          //     { '__attributes.id': 'RUL92' },
          //     { '__attributes.id': 'RUL89' },
          //     { '__attributes.id': 'RUL40' },
          //     { '__attributes.id': 'RUL30' },
          //     { '__attributes.id': 'RUL1' },
          //   ],
          // };
  
          // filter listOfChecks
          // let filteredListOfChecks = mingo.find(listOfChecks, query).all();
  
          const checks = found.map(foundCheck=>{
            return {
              id:foundCheck.rule_id,
              serverity:  foundCheck.serverity
            }
          })
  
          return checks;
        },
        default: null,
      },
      {
        key: 'result.manFiltered',
        transform: function(found, src) {
          const query = {
            $or: [
              { '__attributes.id': 'RUL219' },
              { '__attributes.id': 'RUL116' },
            ],
          };
          const key = 'report.profile_info[0].rules[0].rule';
          let searchElm = getKeyValue(src, key);
          let checks = mingo.find(searchElm, query).all();
          return checks;
        },
        default: null,
      },
    ],
  
    // 'report.results[0]':{
    //     key:'result.checks',
    //     transform:function(value, src){
    //         //let foundChecks = ObjectMapper(value.hits,)
    //         // const query = {
    //         //     "__attributes.id":"RUL1"
    //         // }
    //         const query = {
    //             $or:[
    //                 {"__attributes.id":"RUL1"},
    //                 {"__attributes.id":"RUL219"},
    //             ]
    //         }
    //         const key = "report.profile_info[0].rules[0].rule"
    //         let searchElm = getKeyValue(src, key)
    //         let checks = mingo.find(searchElm,query).all()
    //         let count = mingo.find(searchElm,query).count()
  
    //         return value
    //         // return count
    //         // return checks
    //         // return 1
    //     }
    // }
  }