export type TransformFn = (
  value?: Any,
  fromObject?: AnyObject | AnyArray,
  toObject?: AnyObject | AnyArray | null,
  fromKey?: SourceKey,
  toKey?: Destination
) => Any;

export type DefaultFn = (
  fromObject?: AnyObject | AnyArray,
  fromKey?: SourceKey,
  toObject?: AnyObject | AnyArray,
  toKey?: Destination
) => Any;

export type DestinationKey = string | null;
type SourceKey = string;

export interface DestinationObject {
  key?: DestinationKey;
  transform?: TransformFn;
  default?: DefaultFn | string | number | null;
}

export type DestinationTupel = [
  string | null,
  TransformFn | null,
  DefaultFn | null | string | number
];

// use as map
export interface SourceDestination {
  [SourceKey: string]: Destination;
}
// possible map destinations
export type Destination =
  | DestinationKey
  | [[DestinationKey]]
  | [DestinationKey]
  | DestinationObject
  | [DestinationTupel]
  | (DestinationKey | DestinationTupel | DestinationObject)[];

// context
export interface Context {
  src?: AnyObject;
  srckey?: SourceKey;
  destkey?: Destination; // no key means destination
  transform?: TransformFn;
  default?: DefaultFn | string | number | null;
}

// possible out
export type Expected = AnyArray | AnyObject | null;

export interface AnyObject {
  [property: string]: Any;
}

export type AnyArray = Any[];

export type Any =
  | null
  | string
  | number
  | boolean
  | AnyObject
  | AnyArray
  | undefined;

//
// Parse & Split
export enum Delimiter {
  'DOT' = '.',
  'KOMMA' = ',',
}
export type SplitString = string;
export type SplitArray = string[];

export type ParseString = string;
export type ParseTypes = ObjectKey | ArrayKey | null;
export type ParsedKeys = ParseTypes[];

export interface ObjectKey {
  name: string;
  type: 'object';
  nulls?: boolean;
}

export interface ArrayKey {
  ix: string;
  type: 'array';
  filter?: JSON;
  add?: boolean;
}
