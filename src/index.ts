// export const sum = (a: number, b: number) => {
//   if ('development' === process.env.NODE_ENV) {
//     console.log('boob');
//   }
//   return a + b;
// };

import { ObjectMapper,getKeyValue,setKeyValue,parse,split } from './lib';

export {
  ObjectMapper,
  ObjectMapper as om,
  getKeyValue,
  setKeyValue,
  parse,
  split
}

export default ObjectMapper