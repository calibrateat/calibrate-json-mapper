import mingo from 'mingo';
import {
  Any,
  AnyArray,
  AnyObject,
  ArrayKey,
  ObjectKey,
  ParsedKeys,
} from '../type/types';
import { parse } from './parse';

/**
 * A string of how to navigate through the incoming array is sent.
 * This is translated into an array of instructions for the recursive object
 * @param src
 * @param keystr
 */
export function getKeyValue(src: AnyObject | AnyArray, keystr: string): Any {
  // Parse the source key string into an array/object format that is easy to recurse through
  const keys = parse(keystr);
  // Select the data from the source object or array
  const data = select(src, keys);
  // Return the data for further parsing
  return data;
}

/**
 * With a given source key array, select the corresponding value(s) in the source object/array.
 * If the value does not exist, return null
 * @param src
 * @param keys
 */
function select(src: Any, keys: ParsedKeys | null): Any {
  if (keys === null) return null;
  // Get the object key or index that needs to be parsed
  if (typeof src === 'object' && src !== null) {
    const key = keys.shift() ?? null;
    if (key !== null) {
      switch (key.type) {
        case 'object':
          return selectObj(src, key, keys);
        case 'array':
          return selectArr(src, key, keys);
      }
    }
  }
  return null;
}

/**
 * Loop through the array and select the key from each value in the array.  If nothing comes back, return null
 * @param src
 * @param key
 * @param keys
 */
function selectArr(
  src: AnyArray | AnyObject,
  key: ArrayKey,
  keys: ParsedKeys
): Any {
  const data: AnyArray = [];

  // The source is not an array even though we specify array.  Grab the subnode and add to an array.
  if (!Array.isArray(src)) {
    let d = null;
    if (keys !== null) {
      // Try to get the next value in the chain.  If possible, then add to an array
      if (keys.length) d = select(src, keys);
      // If we found something, return it as an array
      return d !== null ? [d] : null;
    }
    return null;
  } else {
    // check filter option
    if (key.filter) {
      src = filterArrayAccess(src, key.filter);
    }
    // Recursively loop through the array and grab the data
    for (let i = 0; i < src.length; i++) {
      if (keys !== null) {
        const value = src[i];
        // Check to see if we are at a 'leaf' (no more keys to parse).  If so, return the data.  If not, recurse
        const d = keys.length ? select(value, keys.slice()) : value;
        // If the data is populated, add it to the array.  Make sure to keep the same array index so that traversing multi-level arrays work
        if (d !== null) data[i] = d;
      }
    }
  }
  // Return the whole array if a specific index is not defined('') and there is data to return
  if (key.ix === '' && data.length) return data;
  // Return a specific node in the array if defined
  if (key.ix && typeof negativeArrayAccess(data, key.ix) !== 'undefined')
    return negativeArrayAccess(data, key.ix);
  // If we are not expecting an array, return the first node - kinda hack
  // if (typeof data[0] !== 'undefined' && key.name && data[0][key.name])
  //   return data[0][key.name];
  // Otherwise, return nothing
  return null;
}

function filterArrayAccess(arr: AnyArray, filter: JSON): AnyArray {
  const query = new mingo.Query(filter);
  const cursor = query.find(arr);
  return cursor.all();
}

/**
 * Allows negative array indexes to count down from end of array
 * @param arr
 * @param ix
 */
function negativeArrayAccess(arr: AnyArray, ix: string): Any {
  const pix = parseInt(ix);
  return pix < 0 ? arr[arr.length + pix] : arr[pix];
}

/**
 * Traverse the given object for data using the given key arrays
 * @param src
 * @param key
 * @param keys
 */
function selectObj(
  src: AnyArray | AnyObject,
  key: ObjectKey,
  keys: ParsedKeys
): Any {
  // Make sure that there is data where we are looking
  if (src) {
    // && key.name
    // Match all keys in the object
    if (key.name === '*') return selectObjKeys(src, keys);
    // The key specifies an object.  However, the data structure is an array.  Grab the first node and continue
    if (Array.isArray(src)) {
      if (src.length && src[0]) {
        if (keys !== null) {
          const keyName = key.name;
          const srcFirst = src[0];
          let srcFirstName = undefined;
          if (typeof srcFirst === 'object' && !Array.isArray(srcFirst))
            if (keyName in srcFirst) srcFirstName = srcFirst[keyName];
          if (keys.length) {
            return select(srcFirstName, keys);
          } else {
            return srcFirstName;
          }
        }
      } else {
        return null;
      }
    } else if (typeof src === 'object') {
      // The object has the given key
      const _name = key.name;
      if (_name in src) {
        const _value = src[_name];
        if (keys !== null) {
          // There is data to be obtained
          const data = keys.length ? select(_value, keys) : _value;
          // If there is data return it
          if (data !== null) return data;
        }
      }
    }
  }
  // Otherwise, return nothing
  return null;
}

/**
 * Loop through all the keys in the object and select the key from each key in the object.  If nothing comes back, return null
 * @param src
 * @param keys
 */
function selectObjKeys(
  src: AnyObject | AnyArray,
  keys: ParsedKeys
): AnyArray | null {
  const data: AnyArray = [];
  let n = 0;
  // Recursively loop through the object keys and grab the data
  //for (const k in src) {
  for (const v in Object.values(src)) {
    //for (const [k, v] in Object.entries(src)) {
    // Check to see if we are at a 'leaf' (no more keys to parse).  If so, return the data.  If not, recurse
    const d = keys.length ? select(v, keys.slice()) : v;
    //const d = keys.length ? select(v, keys.slice()) : v;
    // If the data is populated, add it to the array
    if (d !== null) data[n++] = d;
  }

  // Return the whole data array if there is data to return
  if (data.length) return data;

  // Otherwise, return nothing
  return null;
}
