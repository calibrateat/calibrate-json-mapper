import {
  ArrayKey,
  Delimiter,
  ObjectKey,
  ParsedKeys,
  ParseString,
} from '../type/types';
import { split } from './split';

//
export function parse(
  parseStr: ParseString | null,
  delimiter: Delimiter = Delimiter.DOT
): ParsedKeys | null {
  // Return null if the key_str is null
  if (parseStr === null || typeof parseStr === 'undefined') return null;
  // Split the key_array and allowing escapes
  const keyArr = split(parseStr, delimiter);
  //const key_arr = key_str.split(delimiter)
  const keys: ParsedKeys = [];
  let n = 0;
  for (let i = 0; i < keyArr.length; i++) {
    // Build a object which is either an object key or an array
    // Note that this is not the most readable, but it is fastest way to parse the string (at this point in time)
    const name_begin = -1;
    let name_end = -1,
      ix_begin = -1,
      ix_end = -1,
      filter_begin = -1,
      filter_end = -1;
    const o: ObjectKey = { name: '', type: 'object' },
      a: ArrayKey = { ix: '', type: 'array' },
      k = keyArr[i];
    for (let j = 0; j < k.length; j++) {
      switch (k[j]) {
        case '[':
          ix_begin = j + 1;
          name_end = j;
          break;
        case ']':
          ix_end = j;
          break;
        case '+':
          if (ix_end === j - 1) a.add = true;
          break;
        case '?':
          name_end = j;
          if (ix_end === -1) o.nulls = true;
          break;
        case '{':
          filter_begin = j;
          break;
        case '}':
          filter_end = j + 1;
          break;
        default:
          if (ix_begin === -1) name_end = j + 1;
      }
    }
    if (name_end > 0) {
      o.name = k.substring(name_begin, name_end);
      keys[n++] = o;
    }
    if (ix_end > 0) {
      if (filter_end > 0) {
        a.filter = safeJsonParse(k.substring(filter_begin, filter_end));
        if (ix_begin === filter_begin) {
          if (ix_end !== filter_end) ix_begin = filter_end + 1;
          else ix_begin = filter_end;
        } else {
          ix_end = filter_begin - 1;
        }
      }
      a.ix = k.substring(ix_begin, ix_end);
      keys[n++] = a;
    }
  }
  return keys;
}

function safeJsonParse(str: string) {
  try {
    return JSON.parse(str);
  } catch {
    console.log(`Filter error with ${str}`);
  }
}
