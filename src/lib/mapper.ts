/* eslint-disable @typescript-eslint/no-explicit-any */
import { AnyObject, Context, Expected, SourceDestination as SourceDestination } from '../type/types';
import { getKeyValue } from './get_key_value';
import { setKeyValue } from './set_key_value';

/**
 * Map a object to another using the passed map
 * @param src
 * @param [dest]
 * @param map
 * @returns {*}
 * @constructor
 */
export function ObjectMapper(
  src: AnyObject | AnyObject[],
  map: SourceDestination
): Expected;
export function ObjectMapper(
  src: AnyObject,
  dest: Expected,
  map: SourceDestination
): Expected;
export function ObjectMapper(src: any, dest: any, map?: any): Expected {
  let destObj: Expected, mapObj: SourceDestination;
  // There are two different constructors - move around properties if needed
  // e.g (ObjectMapper(from,map))
  if (typeof map === 'undefined') {
    mapObj = dest;
    destObj = null;
  } else {
    mapObj = map;
    destObj = dest;
  }
  // let idx = 0
  // Loop through the map to process individual mapping instructions
  for (const [srckey, destination] of Object.entries(mapObj)) {
    // console.log(´idx:{idx}´)
    // idx++
    // Extract the data from the source object or array
    let data = getKeyValue(src, srckey);
    if(data instanceof Array){
      if(data.length > 0){
        //ts-ignore
        data = ([] as any).concat(...data)
      }
    }
    //const data = [...getKeyValue(src, srckey)];
    // Build an object with all of these parameters in case custom transform or default functions need them to derive their values
    const context: Context = { src: src, srckey: srckey, destkey: destination };
    // Set the data into the destination object or array format
    destObj = setKeyValue(destObj, destination, data, context);
  }
  return destObj;
}
