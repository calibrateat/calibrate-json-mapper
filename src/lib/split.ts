import { Delimiter, SplitArray, SplitString } from '../type/types';
// Perform the same function as split(), but keep track of escaped delimiters

export function split(
  splitStr: SplitString,
  delimiter: string = Delimiter.DOT
): string[] {
  const _arr: SplitArray = [];
  let _idx = 0,
    _esc = -99,
    _str = '';
  for (let _i = 0, _len = splitStr.length; _i < _len; _i++) {
    switch (splitStr[_i]) {
      case delimiter:
        if (_esc !== _i - 1) {
          _arr[_idx++] = _str;
          _str = '';
        } else _str += splitStr[_i];
        break;
      case '\\':
        // Escaping a backslash
        if (_esc === _i - 1) {
          _esc = -99;
          _str += splitStr[_i - 1] + splitStr[_i];
        } else _esc = _i;
        break;
      default:
        if (_esc === _i - 1) _str += splitStr[_i - 1];
        _str += splitStr[_i];
    }
  }
  _arr[_idx++] = _str;
  return _arr;
}
