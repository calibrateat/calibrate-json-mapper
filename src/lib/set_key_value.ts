import {
  Any,
  AnyArray,
  AnyObject,
  ArrayKey,
  Context,
  Destination,
  Expected,
  ObjectKey,
  ParseTypes,
  ParsedKeys,
} from '../type/types';
import { parse } from './parse';

/**
 *
 *
 * @param {object} dest
 * @param {string} destination
 * @param {*} data
 * @param {*} [context={default:null,transform:null}]
 * @returns
 */
export function setKeyValue(
  dest: Expected,
  destination: Destination,
  data: Any,
  context: Context = {}
): Expected {
  // Keystr is undefined - call set_data in case there is a default or transformation to deal with
  if (typeof destination === 'undefined' || destination === null)
    return setData(dest, null, data, context);
  // Keystr is an array of values.  Loop through each and identify what format the individual values are
  if (Array.isArray(destination)) {
    for (let i = 0; i < destination.length; i++) {
      const keystrElm = destination[i];
      if (typeof keystrElm === 'string') {
        dest = setKeyValue(dest, keystrElm, data, context);
      }
      // The substring value is in array notation - recurse with the key from the array
      else if (Array.isArray(keystrElm)) {
        const [k, t, d] = keystrElm;
        if (typeof t !== 'undefined' && t !== null) context.transform = t;
        if (typeof d !== 'undefined' && d !== null) context.default = d;
        dest = setKeyValue(dest, k, data, context);
      }
      // The substring value is in object notation - dig further
      else if (typeof keystrElm === 'object' && keystrElm !== null) {
        if (typeof keystrElm.transform !== 'undefined')
          context.transform = keystrElm.transform;
        if (typeof keystrElm.default !== 'undefined')
          context.default = keystrElm.default;
        // If the substring value of the key is an array, parse the array.  If this is parsed in a recursion, it is confused with arrays containing multiple values
        if (Array.isArray(keystrElm.key)) {
          const [k, t, d] = keystrElm.key;
          if (typeof t !== 'undefined') context.transform = t;
          if (typeof d !== 'undefined') context.default = d;
          dest = setKeyValue(dest, k, data, context);
        }
        // The substring value is regular object notation - recurse with the key of the substring
        else {
          const key = keystrElm.key;
          dest = setKeyValue(dest, key ?? null, data, context);
        }
      }
    }
  }
  // The value is in string notation - ready for update!
  else if (typeof destination === 'string') {
    dest = update(dest, data, parse(destination), context);
  }
  // The value is in object notation - dig a bit further
  else {
    if (typeof destination.transform !== 'undefined')
      context.transform = destination.transform;
    if (typeof destination.default !== 'undefined')
      context.default = destination.default;
    // If the value of the key is an array, parse the array.  If this is parsed in a recursion, it is confused with arrays containing multiple values
    if (Array.isArray(destination.key)) {
      const [k, t, d] = destination.key;
      if (typeof t !== 'undefined' && t !== null) context.transform = t;
      if (typeof d !== 'undefined' && d !== null) context.default = d;
      dest = setKeyValue(dest, k, data, context);
    }
    // The value is in regular object notation.  Recurse with the object key
    else dest = setKeyValue(dest, destination.key ?? null, data, context);
  }

  return dest;
}

// Set the given data into the given destination object
function setData(
  dest: AnyObject | AnyArray | null,
  key: ParseTypes | null,
  data: Any,
  context: Context
): Expected {
  // | AnyArray | null {
  // See if data is null and there is a default
  if (
    typeof context.default !== 'undefined' &&
    (data === null || typeof data === 'undefined')
  ) {
    // There is a default function, call the function to set the default
    if (typeof context.default === 'function') {
      dest = dest || {};
      data =
        context.default(context.src, context.srckey, dest, context.destkey) ??
        null;
    }
    // The default is a specific value
    else data = context.default;
  }

  // If there is a transformation function, call the function.
  if (typeof context.transform === 'function') {
    dest = dest || {};
    data =
      context.transform(
        data,
        context.src,
        dest,
        context.srckey,
        context.destkey
      ) ?? null;
  }

  // Set the object to the data if it is not undefined
  // if (typeof data !== 'undefined' && key) {
  if (data != null && key) {
    if (key.type === 'object') {
      // Set the data if the data is not null, or if the 'allow nulls' key is set, or if there is a default (in the case of default=null, make sure to write this out)
      if (
        data != null ||
        key.nulls ||
        (typeof context.default !== 'undefined' && context.default === null)
      ) {
        if (dest === null || typeof dest === 'undefined') dest = {};
        dest = dest || {};
        const targetName = key.name;
        if (!Array.isArray(dest)) dest[targetName] = data;
      }
    }
  }

  // Return the dest variable back to the caller.
  return dest;
}

/**
 * if the data is an array, walk down the obj path and build until there is an array key
 * @param dest
 * @param value
 * @param keys
 * @param context
 */
function update(
  dest: Expected,
  value: Any,
  keys: ParsedKeys | null,
  context: Context
): Expected {
  if (keys) {
    // Get the object key and index that needs to be parsed
    const key = keys.shift();
    if (
      //   typeof dest === 'object' &&
      //   dest !== null &&
      key != null // && typeof key !== 'undefined'
    ) {
      switch (key.type) {
        case 'object':
          //if (!Array.isArray(dest))
          return updateObj(dest, key, value, keys, context);
        //break;
        case 'array':
          if (Array.isArray(dest)){ 
            return updateArr(dest, key, value, keys, context);
          } else {
              return updateArr([], key, value, keys, context);
          }
        //break;
      }
    }
  }
  return setData(dest, null, value, context);
}

/**
 * Update the destination object.key with the data
 * @param dest
 * @param key
 * @param value
 * @param keys
 * @param context
 */
// Update the destination object.key with the data
function updateObj(
  dest: Expected,
  key: ObjectKey,
  data: Any,
  keys: ParsedKeys,
  context: Context
): Expected {
  // There are further instructions remaining - we will need to recurse
  if (keys.length) {
    // There is a pre-existing destination object.  Recurse through to the object key
    if (
      dest != null &&
      // typeof dest !== 'undefined' &&
      !Array.isArray(dest)
      // && key.name in dest
    ) {
      let givenDest = null;
      if (key.name in dest) {
        const found = dest[key.name];
        if (typeof found !== 'object') givenDest = null;
        else givenDest = found;
      }
      const o = update(givenDest, data, keys, context); // ??
      if (o != null){ // && typeof o !== 'undefined') {
        dest[key.name] = o;
      }
      // ToDo: Change
      // else{
      //   console.log(dest[key.name])
      //   // delete dest[key.name] // ??
      //   return dest;
      // }

      // const o =
      //   typeof givenDest === 'object'
      //     ? update(givenDest, data, keys, context)
      //     : null;
      // if (o !== null && typeof o !== 'undefined') dest[key.name] = o;
    }
    // There is no pre-existing object.  Check to see if data exists before creating a new object
    else {
      // Check to see if there is a value before creating an object to store it
      const o = update(null, data, keys, context);
      if (o !== null) {
        dest = {};
        dest[key.name] = o;
      }
    }
  }
  // This is a leaf.  Set data into the dest
  else dest = setData(dest, key, data, context);

  return dest;
}

/**
 * Update the dest[] array with the data on each index
 * @param dest
 * @param key
 * @param value
 * @param keys
 * @param context
 */
function updateArr(
  dest: AnyArray | null,
  key: ArrayKey,
  data: Any,
  keys: ParsedKeys,
  context: Context
): Expected {
  // The 'add' instruction is set.  This means to take the data and add it onto a new array node
  if (key.add) {
    if (data != null){ //&& typeof data !== 'undefined') {
      dest = dest || [];
      dest.push(applyTransform(data, dest, context));
      // dest = dest.concat(data)
    }
    return dest;
  }
  // Just update a single array node
  if (key.ix !== '') {
    return updateArrIx(
      dest,
      parseInt(key.ix),
      applyTransform(data, dest, context),
      keys,
      context
    );
  }
  // If the data is in an array format then make sure that there is a dest index for each data index
  if (Array.isArray(data)) {
    if (dest === null || typeof dest === 'undefined') dest = [];
    // Loop through each index in the data array and update the destination object with the data
    dest = data.reduce(
      (dest: AnyArray | null, d: Any, i: number): AnyArray | null => {
        // If the instruction is to update all array indices ('') or the current index, update the child data element.  Otherwise, don't bother
        if (key.ix === '' || parseInt(key.ix) === i) {
          return updateArrIx(
            dest,
            i,
            applyTransform(d, dest, context),
            keys.slice(),
            context
          );
        } else {
          return [];
        }
      },
      dest
    );
    return dest;
  }
  // Set the specific array index with the data
  else return updateArrIx(dest, 0, data, keys, context);
}

function applyTransform(
  data: Any,
  dest: AnyArray | null,
  context: Context
): Any {
  if (typeof context.transform === 'function')
    return context.transform(
      data,
      context.src,
      dest,
      context.srckey,
      context.destkey
    );
  else return data;
}

function updateArrIx(
  dest: AnyArray | null,
  ix: number,
  data: Any,
  keys: ParsedKeys,
  context: Context
): AnyArray | null {
  let o: Any;
  // ToDo: Change
  if (dest != null){ //&& typeof dest !== 'undefined') {
    const givenDest = dest[ix] ?? null;
    if (keys.length && (typeof givenDest === 'object' || givenDest === null)) {
      o = update(givenDest, data, keys, context);
    } else {
      o = data;
    }
  } else {
    o = keys.length ? update(null, data, keys, context) : data;
  }
  // Only update (and create if needed) dest if there is data to be saved
  // ToDo: Change
  if (o != null) {
    dest = dest || [];
    dest[ix] = o;
  }
  return dest;
}
