const dataArr = ['a','b','c']
const dataObj = {d:1,e:[23,11],f:{g:17}}

let map_001 =  dataArr.map(x => x );
console.log(map_001);
console.log(Object.entries(dataArr)); // [ ['0', 'a'], ['1', 'b'], ['2', 'c'] ]

let map_002 = new Map(Object.entries(dataObj));
console.log(map_002);
console.log(Object.entries(dataObj)); // [ ['0', 'a'], ['1', 'b'], ['2', 'c'] ]

