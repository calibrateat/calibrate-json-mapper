/* eslint-env jest */
import { parse } from '../src';

describe('Parse', () => {
  it('complicated key', () => {
    const test = 'abc[].def[42]+.ghi?.j..k\\.l\\\\.m.';
    const expected = [
      { type: 'object', name: 'abc' },
      { type: 'array', ix: '' },
      { type: 'object', name: 'def' },
      { type: 'array', ix: '42', add: true },
      { type: 'object', name: 'ghi', nulls: true },
      { type: 'object', name: 'j' },
      { type: 'object', name: 'k.l\\\\' },
      { type: 'object', name: 'm' },
    ];
    expect(parse(test)).toEqual(expected);
  });

  it('PARSE with simple key', () => {
    const test = 'abc';
    const expected = [{ type: 'object', name: 'abc' }];
    expect(parse(test)).toEqual(expected);
  });

  it('PARSE abc? (simple key allowing nulls)', () => {
    const test = 'abc?';
    const expected = [{ type: 'object', name: 'abc', nulls: true }];
    expect(parse(test)).toEqual(expected);
  });

  it('PARSE with simple empty array key', () => {
    const test = 'abc[]';
    const expected = [
      { type: 'object', name: 'abc' },
      { type: 'array', ix: '' },
    ];
    expect(parse(test)).toEqual(expected);
  });

  it('PARSE with simple empty array key with plus', () => {
    const test = 'abc[]+';
    const expected = [
      { type: 'object', name: 'abc' },
      { type: 'array', ix: '' , "add": true},
    ];
    expect(parse(test)).toEqual(expected);
  });

  it('PARSE with no key empty array key', () => {
    const test = '[]';
    const expected = [{ type: 'array', ix: '' }];
    expect(parse(test)).toEqual(expected);
  });

  it('PARSE with nothing', () => {
    const test = '';
    const expected: object[] = [];
    expect(parse(test)).toEqual(expected);
  });

  it('PARSE with simple dot notation key', () => {
    const test = 'abc.def';
    const expected = [
      { type: 'object', name: 'abc' },
      { type: 'object', name: 'def' },
    ];
    expect(parse(test)).toEqual(expected);
  });

  it('PARSE with deep dot notation key', () => {
    const test = 'a.b.c.d.e.f';
    const expected = [
      { type: 'object', name: 'a' },
      { type: 'object', name: 'b' },
      { type: 'object', name: 'c' },
      { type: 'object', name: 'd' },
      { type: 'object', name: 'e' },
      { type: 'object', name: 'f' },
    ];
    expect(parse(test)).toEqual(expected);
  });

  it('PARSE with deep brackets 1/2', () => {
    const test = 'abc[].def';
    const expected = [
      { type: 'object', name: 'abc' },
      { type: 'array', ix: '' },
      { type: 'object', name: 'def' },
    ];
    expect(parse(test)).toEqual(expected);
  });

  it('PARSE with deep brackets and instruction to add together', () => {
    const test = 'abc[]+.def';
    const expected = [
      { type: 'object', name: 'abc' },
      { type: 'array', ix: '', add: true },
      { type: 'object', name: 'def' },
    ];
    expect(parse(test)).toEqual(expected);
  });

  it('PARSE with deep brackets and instruction to add nulls', () => {
    const test = 'abc[]+.def?';
    const expected = [
      { type: 'object', name: 'abc' },
      { type: 'array', ix: '', add: true },
      { type: 'object', name: 'def', nulls: true },
    ];
    expect(parse(test)).toEqual(expected);
  });

  it('PARSE with deep brackets 2/2', () => {
    const test = '[].def';
    const expected = [
      { type: 'array', ix: '' },
      { type: 'object', name: 'def' },
    ];
    expect(parse(test)).toEqual(expected);
  });

  it('PARSE with filter in brackets', () => {
    const test = '[{"id":5}].def';
    const expected = [
      { type: 'array', ix: '', filter: { id: 5 } },
      { type: 'object', name: 'def' },
    ];
    const parsed = parse(test);
    expect(parsed).toEqual(expected);
  });

  it('PARSE with filter in brackets and ix in front', () => {
    const test = '[32,{"id":5}].def';
    const expected = [
      { type: 'array', ix: '32', filter: { id: 5 } },
      { type: 'object', name: 'def' },
    ];
    const parsed = parse(test);
    expect(parsed).toEqual(expected);
  });

  it('PARSE with filter in brackets and ix at end', () => {
    const test = '[{"id":5},6].def';
    const expected = [
      { type: 'array', ix: '6', filter: { id: 5 } },
      { type: 'object', name: 'def' },
    ];
    const parsed = parse(test);
    expect(parsed).toEqual(expected);
  });

  it('get value - filter in middle', () => {
    const test = 'foo[{"id":23}].fog';
    const expected = [
      { type: 'object', name: 'foo' },
      { type: 'array', ix: '', filter: { id: 23 } },
      { type: 'object', name: 'fog' },
    ];
    const parsed = parse(test);
    expect(parsed).toEqual(expected);
  });
});
