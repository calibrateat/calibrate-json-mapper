// import { parse, getKeyValue } from '../src';
import {ObjectMapper} from '../src'

describe ('ich du er',() => {

    const incomeData = {
        'collection1': [{
          'object': '1',
          'collection2': [
            { 'name1': 'ich' },
            { 'name1': 'du' }
          ]
        },
        {
          'object': '2',
          'collection2': [
            {
              'name1': 'er'
            }
          ]
        }]
      }

      const map_001 = {
        'collection1[].collection2[].name1':
          {
            "key": 'Collection3[]',
            transform: function (_value: any, _src: any, _dest: any, _destKey: any) {
              if(_value !== null) {
                return _value
              }
            }
          }
      }
      const expected_001 = {"Collection3": ['ich', 'du','er']}
      
      const map_002 = {
        'collection1[].collection2[]':
          {
            "key": 'Collection3[]',
            transform: function (_value: any, _src: any, _dest: any, _destKey: any) {
              if(typeof _value === 'object') {
                if(_value .hasOwnProperty('name1'))
                  return _value['name1']
              }
            }
          }
      }
      
      const expected_003 = {"Collection3": []}
      
      const map_003 = {
        'collection1[].collection2[]':
          {
            "key": 'Collection3[]',
            transform: function (_value: any, _src: any, _dest: any, _destKey: any) {
              return null
            }
          }
      }

      const expected_004 = {"Collection3": []}
      
      const map_004 = {
        'collection1[].notDefined':
          {
            "key": 'Collection3[]',
            transform: function (_value: any, _src: any, _dest: any, _destKey: any) {
              return null
            }
          }
      }

  // ToDo: should be rewrite to transform.test.ts
    it('map_001', async () => {
        let mapped = ObjectMapper(incomeData, {}, map_001);
        expect(mapped).toStrictEqual(expected_001)
    })

    // ToDo: should be rewrite to transform.test.ts
    it('map_002', async () => {
        let mapped = ObjectMapper(incomeData, {}, map_002);
        expect(mapped).toStrictEqual(expected_001)
    })

    // ToDo: should be rewrite to transform.test.ts
    it('map_003', async () => {
        let mapped = ObjectMapper(incomeData, {}, map_003);
        expect(mapped).toStrictEqual(expected_003)
    })
  
    // ToDo: should be rewrite to transform.test.ts
    it('map_004', async () => {
        let mapped = ObjectMapper(incomeData, {}, map_004);
        expect(mapped).toStrictEqual(expected_004)
    })


})