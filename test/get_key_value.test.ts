/* eslint-env jest */
/* eslint-disable no-alert */
/* eslint-disable no-sparse-arrays */

import { getKeyValue } from '../src';

describe('GetKeyValue', () => {
  it('get value - one level deep', () => {
    const source = { foo: { bar: 'baz' } };
    const key = 'foo.bar';
    const expected = 'baz';
    expect(getKeyValue(source, key)).toEqual(expected);
  });

  it('get value - starting with simple array', () => {
    const source = ['bar'];
    const key = '[]';
    const expected = ['bar'];
    expect(getKeyValue(source, key)).toEqual(expected);
  });

  it('get value - simple array defined index', () => {
    const source = ['foo', 'bar'];
    const key = '[1]';
    const expected = 'bar';
    expect(getKeyValue(source, key)).toEqual(expected);
  });

  it('get value - simple array negative index', () => {
    const source = ['foo', 'bar'];
    const key = '[-1]';
    const expected = 'bar';
    expect(getKeyValue(source, key)).toEqual(expected);
  });

  it('get value - simple array dot property', () => {
    const source = [{ name: 'foo' }, { name: 'bar' }];
    const key = '[-1].name';
    const expected = 'bar';
    expect(getKeyValue(source, key)).toEqual(expected);
  });

  it('get value - simple array negative index falls off array', () => {
    const source = ['foo', 'bar'];
    const key = '[-3]';
    const expected = null;
    expect(getKeyValue(source, key)).toEqual(expected);
  });

  it('get value - two levels deep', () => {
    const source = {
      foo: {
        baz: {
          fog: 'bar',
        },
      },
    };
    const key = 'foo.baz.fog';
    const expected = 'bar';
    expect(getKeyValue(source, key)).toEqual(expected);
  });

  it('get value - one level deep and item is a array', () => {
    const key = 'foo.baz[]';
    const source = {
      foo: {
        baz: ['bar'],
      },
    };
    const expected = ['bar'];
    expect(getKeyValue(source, key)).toEqual(expected);
  });

  it('get value - one level deep and first item of array', () => {
    const source = {
      foo: {
        baz: ['bar', 'foo'],
      },
    };
    const key = 'foo.baz[1]';
    const expected = 'foo';
    expect(getKeyValue(source, key)).toEqual(expected);
  });

  it('get value - one level deep and array and one level', () => {
    const source = {
      foo: {
        baz: [
          {
            fog: 'bar',
          },
        ],
      },
    };
    const key = 'foo.baz[].fog';
    const expected = ['bar'];
    expect(getKeyValue(source, key)).toEqual(expected);
  });

  it('get value - one level deep and first item of array and one level', () => {
    const source = {
      foo: {
        baz: [
          {
            fog: 'bar',
          },
        ],
      },
    };
    const key = 'foo.baz[0].fog';
    const expected = 'bar';
    expect(getKeyValue(source, key)).toEqual(expected);
  });

  it('get value - one level deep and first item of array and two levels', () => {
    const source = {
      foo: {
        baz: [
          {
            fog: {
              baz: 'bar',
            },
          },
        ],
      },
    };
    const key = 'foo.baz[0].fog.baz';
    const expected = 'bar';
    expect(getKeyValue(source, key)).toEqual(expected);
  });

  it('get value - one level array', () => {
    const source = {
      foo: [
        {
          baz: [
            {
              fog: {
                baz: 'bar',
              },
            },
          ],
        },
      ],
    };
    const key = 'foo[]';
    const expected = [
      {
        baz: [
          {
            fog: {
              baz: 'bar',
            },
          },
        ],
      },
    ];
    expect(getKeyValue(source, key)).toEqual(expected);
  });

  it('get value - two level deep array', () => {
    const source = {
      foo: [{ baz: [{ fog: { baz: 'bar' } }, { fog: { baz: 'var' } }] }],
    };
    const key = 'foo[].baz[].fog.baz';
    const expected = [['bar', 'var']];
    expect(getKeyValue(source, key)).toEqual(expected);
  });

  it('get value - crazy', () => {
    const source = {
      foo: {
        baz: [
          {
            fog: [
              ,
              {
                baz: 'bar',
              },
            ],
          },
        ],
      },
    };

    const key = 'foo.baz[0].fog[1].baz';
    const expected = 'bar';

    expect(getKeyValue(source, key)).toEqual(expected);
  });

  it('get value - crazy negative', () => {
    const source = {
      foo: {
        baz: [
          {
            fog: [
              ,
              {
                baz: 'bar',
              },
            ],
          },
        ],
      },
    };
    const key = 'foo.baz[-1].fog[1].baz';
    const expected = 'bar';
    expect(getKeyValue(source, key)).toEqual(expected);
  });

  it('select with array object where key is not an array 1', () => {
    const source = { foo: [{ bar: 'a' }, { bar: 'b' }, { bar: 'c' }] };
    const key = 'foo.bar';
    const expected = 'a';
    expect(getKeyValue(source, key)).toEqual(expected);
  });

  it('select with array object where key is not an array 2', () => {
    const source = { foo: [{ bar: 'a' }, { bar: 'b' }, { bar: 'c' }] };
    const key = 'foo[].bar';
    const expected = ['a', 'b', 'c'];
    expect(getKeyValue(source, key)).toEqual(expected);
  });

  it('Ensure that multi-dimensional arrays work', () => {
    const source = {
      arr: [
        {
          id: 1,
        },
      ],
    };
    const key = 'arr[].arr[].id';
    const expected = null;
    expect(getKeyValue(source, key)).toEqual(expected);
  });

  it('get value - index on array', () => {
    const source = {
      foo: {
        baz: [
          {
            id: 23,
            fog: '23',
          },
          {
            id: 24,
            fog: '24',
          },
          {
            id: 25,
            fog: '25',
          },
        ],
      },
    };
    const key = 'foo.baz[].fog';
    const expected = ['23', '24', '25'];
    expect(getKeyValue(source, key)).toEqual(expected);
  });

  it('get value - filter array out', () => {
    const source = {
      foo: {
        baz: [
          {
            id: 23,
            fog: '23',
          },
          {
            id: 24,
            fog: '24',
          },
          {
            id: 24,
            fog: '24',
          },
          {
            id: 25,
            fog: '25',
          },
        ],
      },
    };
    const key = 'foo.baz[{"id":24}]';
    const expected = [
      {
        id: 24,
        fog: '24',
      },
      {
        id: 24,
        fog: '24',
      },
    ];
    expect(getKeyValue(source, key)).toEqual(expected);
  });

  it('get value - filter and idx on array', () => {
    const source = {
      foo: {
        baz: [
          {
            id: 23,
            fog: '23',
          },
          {
            id: 24,
            fog: '24',
          },
          {
            id: 24,
            fog: '24',
          },
          {
            id: 25,
            fog: '25',
          },
        ],
      },
    };
    const key = 'foo.baz[0,{"id":24}]';
    const expected = { fog: '24', id: 24 };
    expect(getKeyValue(source, key)).toEqual(expected);
  });

  it('get value - filter and idx on array and property select', () => {
    const source = {
      foo: {
        baz: [
          {
            id: 23,
            fog: '23',
          },
          {
            id: 24,
            fog: '24',
          },
          {
            id: 24,
            fog: '24+',
          },
          {
            id: 25,
            fog: '25',
          },
        ],
      },
    };
    const key = 'foo.baz[{"id":24}].fog';
    const expected = ['24','24+'];
    expect(getKeyValue(source, key)).toEqual(expected);
  });

  it('get value - filter on array and property select', () => {
    const source = {
      foo: {
        baz: [
          {
            id: 23,
            fog: '23',
          },
          {
            id: 24,
            fog: '24',
          },
          {
            id: 24,
            fog: '24',
          },
          {
            id: 25,
            fog: '25',
          },
        ],
      },
    };
    const key = 'foo.baz[{"id":24}].fog';
    const expected = ['24', '24'];
    const result = getKeyValue(source, key);
    expect(result).toEqual(expected);
  });
});
