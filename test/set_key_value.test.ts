/* eslint-disable no-sparse-arrays */
/* eslint-env jest */
import { setKeyValue } from '../src';

describe('SetKeyValue', () => {
  it('set value - simple', () => {
    const key = 'foo';
    const value = 'bar';
    const expected = {
      foo: 'bar',
    };
    expect(setKeyValue(null, key, value)).toEqual(expected);
  });

  it('set value - simple with base object', () => {
    const key = 'foo';
    const value = 'bar';
    const base = {
      baz: 'foo',
    };
    const expected = {
      baz: 'foo',
      foo: 'bar',
    };
    expect(setKeyValue(base, key, value)).toEqual(expected);
  });

  it('set value - simple array', () => {
    const key = '[]';
    const value = 'bar';
    const expected = ['bar'];
    expect(setKeyValue(null, key, value)).toEqual(expected);
  });

  it('set value - simple array with base array', () => {
    const key = '[]';
    const value = 'bar';
    const base = ['foo'];
    const expected = ['bar'];
    expect(setKeyValue(base, key, value)).toEqual(expected);
  });

  it('set value - simple array in index 0', () => {
    const value = 'bar';
    const map = '[0]';
    const expected = ['bar'];
    expect(setKeyValue(null, map, value)).toEqual(expected);
  });

  it('set value - simple array in index 0 with base array', () => {
    const key = '[0]';
    const value = 'bar';
    const base = ['foo'];
    const expected = ['bar'];
    expect(setKeyValue(base, key, value)).toEqual(expected);
  });

  it('set value - simple array in index 1', () => {
    const value = 'bar';
    const key = '[1]';
    const expected = [, 'bar'];
    expect(setKeyValue(null, key, value)).toEqual(expected);
  });

  it('set value - one level deep', () => {
    const key = 'foo.bar';
    const value = 'baz';
    const expected = {
      foo: {
        bar: 'baz',
      },
    };
    expect(setKeyValue({}, key, value)).toEqual(expected);
  });

  it('set value - object inside simple array', () => {
    const key = '[].foo';
    const value = 'bar';
    const expected = [
      {
        foo: 'bar',
      },
    ];
    expect(setKeyValue(null, key, value)).toEqual(expected);
  });

  it('set value - array to object inside simple array', () => {
    const key = '[].foo';
    const value = ['bar', 'baz'];
    const expected = [
      {
        foo: 'bar',
      },
      {
        foo: 'baz',
      },
    ];
    expect(setKeyValue(null, key, value)).toEqual(expected);
  });

  it('set value - object inside simple array defined index', () => {
    const value = 'bar';
    const key = '[3].foo';
    const expected = [
      ,
      ,
      ,
      {
        foo: 'bar',
      },
    ];
    expect(setKeyValue(null, key, value)).toEqual(expected);
  });

  it('set value - two levels deep', () => {
    const key = 'foo.bar.baz';
    const value = 'foo';
    const expected = {
      foo: {
        bar: {
          baz: 'foo',
        },
      },
    };
    expect(setKeyValue({}, key, value)).toEqual(expected);
  });

  it('set value - one level deep inside array', () => {
    const key = 'foo.bar[]';
    const value = 'baz';
    const expected = {
      foo: {
        bar: ['baz'],
      },
    };
    expect(setKeyValue({}, key, value)).toEqual(expected);
  });

  it('set value - one level deep inside array with one level deep', () => {
    const key = 'foo.bar[].baz';
    const value = 'foo';
    const expected = {
      foo: {
        bar: [
          {
            baz: 'foo',
          },
        ],
      },
    };
    expect(setKeyValue({}, key, value)).toEqual(expected);
  });

  it('set value - one level deep inside array with one level deep inside a existing array', () => {
    const key = 'foo.bar[].baz';
    const value = 'foo';
    const base = {
      foo: {
        bar: [
          {
            bar: 'baz',
          },
        ],
      },
    };
    const expected = {
      foo: {
        bar: [
          {
            bar: 'baz',
            baz: 'foo',
          },
        ],
      },
    };
    expect(setKeyValue(base, key, value)).toEqual(expected);
  });

  it('set value - one level deep inside array at defined index with one level deep', () => {
    const key = 'foo.bar[1].baz';
    const value = 'foo';
    const expected = {
      foo: {
        bar: [
          ,
          {
            baz: 'foo',
          },
        ],
      },
    };
    expect(setKeyValue({}, key, value)).toEqual(expected);
  });

  it('set value - array to simple object', () => {
    const key = 'foo[].baz';
    const value = ['foo', 'var'];
    const expected = {
      foo: [
        {
          baz: 'foo',
        },
        {
          baz: 'var',
        },
      ],
    };
    const result = setKeyValue({}, key, value);
    expect(result).toEqual(expected);
  });

  it('set value - array to two level object', () => {
    const key = 'bar.foo[].baz';
    const value = ['foo', 'var'];
    const expected = {
      bar: {
        foo: [
          {
            baz: 'foo',
          },
          {
            baz: 'var',
          },
        ],
      },
    };
    expect(setKeyValue({}, key, value)).toEqual(expected);
  });

  it('set value - array to two level object', () => {
    const key = 'bar.foo[].baz.foo';
    const value = ['foo', 'var'];
    const expected = {
      bar: {
        foo: [
          {
            baz: {
              foo: 'foo',
            },
          },
          {
            baz: {
              foo: 'var',
            },
          },
        ],
      },
    };
    expect(setKeyValue({}, key, value)).toEqual(expected);
  });

  // it('set value - array to object', () => {
  //   const key = 'foo[].bar[].baz';
  //   const value = [['foo', 'var']];
  //   const expected = {
  //     foo: [
  //       {
  //         bar: [
  //           {
  //             baz: 'foo',
  //           },
  //           {
  //             baz: 'var',
  //           },
  //         ],
  //       },
  //     ],
  //   };
  //   expect(setKeyValue({}, key, value)).toEqual(expected);
  // });

  // it('set value - crazy', () => {
  //   const key = 'foo.bar[1].baz[2].thing';
  //   const value = 'foo';
  //   const expected = {
  //     foo: {
  //       bar: [
  //         ,
  //         {
  //           baz: [
  //             ,
  //             ,
  //             {
  //               thing: 'foo',
  //             },
  //           ],
  //         },
  //       ],
  //     },
  //   };
  //   expect(setKeyValue({}, key, value)).toEqual(expected);
  // });
});
