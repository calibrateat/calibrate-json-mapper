# map handling

## js module

```js
module.export = {
  'Joberstellung.JobNr[0]': 'ticket.jobNr',
  'Joberstellung.ProdInfo01[0]': {
    key: 'ticket.poduct',
    translation: function(value) {
      const infoArr = value.split(' ');
      return {
        type: infoArr[0],
        format: infoArr[1],
        weight: infoArr[2],
      };
    },
    default: null,
  },
};
```

## json

### write sample

```js
const fs = require('fs');

const mapJSON = {
  'Joberstellung.JobNr[0]': 'ticket.jobNr',
  'Joberstellung.ProdInfo01[0]': {
    key: 'ticket.poduct',
    translation: function(value) {
      const infoArr = value.split(' ');
      return {
        type: infoArr[0],
        format: infoArr[1],
        weight: infoArr[2],
      };
    },
    default: null,
  },
};

function replacer(key, value) {
  // Filtering out properties
  if (typeof value === 'function') return '__func__' + encodeURI(value);
  else return value;
}

const mapString = JSON.stringify(mapJSON, replacer);

fs.writeFile('./test/resources/json_map.json', mapString, err => {
  console.log(err);
});
```

### read sample

```js
const fs = require('fs');
var _func = null;
function reviver(key, value) {
  if (this.__func) {
    try {
      this[this.__func] = eval('(' + this[this.__func] + ')');
    } catch (err) {
      console.log(err);
    }
    this.__func = null;
  }
  if (typeof value === 'string') {
    if (value.substring(0, 8) === '__func__') {
      value = decodeURI(value.substring(8, value.length));
      this.__func = key;
    }
  }
  return value;
}

let map;
fs.readFile('./test/resources/json_map.json', (err, data) => {
  if (err) console.log(err);
  map = JSON.parse(data, reviver);
});
```
