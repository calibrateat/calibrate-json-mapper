
'use strict'

if (process.env.NODE_ENV === 'production') {
  module.exports = require('./calibrate-json-mapper.cjs.production.min.js')
} else {
  module.exports = require('./calibrate-json-mapper.cjs.development.js')
}
