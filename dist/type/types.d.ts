export declare type TransformFn = (value?: Any, fromObject?: AnyObject | AnyArray, toObject?: AnyObject | AnyArray | null, fromKey?: SourceKey, toKey?: Destination) => Any;
export declare type DefaultFn = (fromObject?: AnyObject | AnyArray, fromKey?: SourceKey, toObject?: AnyObject | AnyArray, toKey?: Destination) => Any;
export declare type DestinationKey = string | null;
declare type SourceKey = string;
export interface DestinationObject {
    key?: DestinationKey;
    transform?: TransformFn;
    default?: DefaultFn | string | number | null;
}
export declare type DestinationTupel = [string | null, TransformFn | null, DefaultFn | null | string | number];
export interface SourceDestination {
    [SourceKey: string]: Destination;
}
export declare type Destination = DestinationKey | [[DestinationKey]] | [DestinationKey] | DestinationObject | [DestinationTupel] | (DestinationKey | DestinationTupel | DestinationObject)[];
export interface Context {
    src?: AnyObject;
    srckey?: SourceKey;
    destkey?: Destination;
    transform?: TransformFn;
    default?: DefaultFn | string | number | null;
}
export declare type Expected = AnyArray | AnyObject | null;
export interface AnyObject {
    [property: string]: Any;
}
export declare type AnyArray = Any[];
export declare type Any = null | string | number | boolean | AnyObject | AnyArray | undefined;
export declare enum Delimiter {
    'DOT' = ".",
    'KOMMA' = ","
}
export declare type SplitString = string;
export declare type SplitArray = string[];
export declare type ParseString = string;
export declare type ParseTypes = ObjectKey | ArrayKey | null;
export declare type ParsedKeys = ParseTypes[];
export interface ObjectKey {
    name: string;
    type: 'object';
    nulls?: boolean;
}
export interface ArrayKey {
    ix: string;
    type: 'array';
    filter?: JSON;
    add?: boolean;
}
export {};
