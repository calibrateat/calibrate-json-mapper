import { AnyObject, Expected, SourceDestination as SourceDestination } from '../type/types';
/**
 * Map a object to another using the passed map
 * @param src
 * @param [dest]
 * @param map
 * @returns {*}
 * @constructor
 */
export declare function ObjectMapper(src: AnyObject | AnyObject[], map: SourceDestination): Expected;
export declare function ObjectMapper(src: AnyObject, dest: Expected, map: SourceDestination): Expected;
