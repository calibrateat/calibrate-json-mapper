import { Delimiter, ParsedKeys, ParseString } from '../type/types';
export declare function parse(parseStr: ParseString | null, delimiter?: Delimiter): ParsedKeys | null;
