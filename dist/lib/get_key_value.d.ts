import { Any, AnyArray, AnyObject } from '../type/types';
/**
 * A string of how to navigate through the incoming array is sent.
 * This is translated into an array of instructions for the recursive object
 * @param src
 * @param keystr
 */
export declare function getKeyValue(src: AnyObject | AnyArray, keystr: string): Any;
