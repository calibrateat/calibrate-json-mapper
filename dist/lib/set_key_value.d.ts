import { Any, Context, Destination, Expected } from '../type/types';
/**
 *
 *
 * @param {object} dest
 * @param {string} destination
 * @param {*} data
 * @param {*} [context={default:null,transform:null}]
 * @returns
 */
export declare function setKeyValue(dest: Expected, destination: Destination, data: Any, context?: Context): Expected;
