import { SplitString } from '../type/types';
export declare function split(splitStr: SplitString, delimiter?: string): string[];
