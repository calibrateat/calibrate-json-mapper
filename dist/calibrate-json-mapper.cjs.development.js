'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var mingo = _interopDefault(require('mingo'));

//
// Parse & Split
var Delimiter;

(function (Delimiter) {
  Delimiter["DOT"] = ".";
  Delimiter["KOMMA"] = ",";
})(Delimiter || (Delimiter = {}));

function split(splitStr, delimiter) {
  if (delimiter === void 0) {
    delimiter = Delimiter.DOT;
  }

  var _arr = [];

  var _idx = 0,
      _esc = -99,
      _str = '';

  for (var _i = 0, _len = splitStr.length; _i < _len; _i++) {
    switch (splitStr[_i]) {
      case delimiter:
        if (_esc !== _i - 1) {
          _arr[_idx++] = _str;
          _str = '';
        } else _str += splitStr[_i];

        break;

      case '\\':
        // Escaping a backslash
        if (_esc === _i - 1) {
          _esc = -99;
          _str += splitStr[_i - 1] + splitStr[_i];
        } else _esc = _i;

        break;

      default:
        if (_esc === _i - 1) _str += splitStr[_i - 1];
        _str += splitStr[_i];
    }
  }

  _arr[_idx++] = _str;
  return _arr;
}

function parse(parseStr, delimiter) {
  if (delimiter === void 0) {
    delimiter = Delimiter.DOT;
  }

  // Return null if the key_str is null
  if (parseStr === null || typeof parseStr === 'undefined') return null; // Split the key_array and allowing escapes

  var keyArr = split(parseStr, delimiter); //const key_arr = key_str.split(delimiter)

  var keys = [];
  var n = 0;

  for (var i = 0; i < keyArr.length; i++) {
    // Build a object which is either an object key or an array
    // Note that this is not the most readable, but it is fastest way to parse the string (at this point in time)
    var name_begin = -1;
    var name_end = -1,
        ix_begin = -1,
        ix_end = -1,
        filter_begin = -1,
        filter_end = -1;
    var o = {
      name: '',
      type: 'object'
    },
        a = {
      ix: '',
      type: 'array'
    },
        k = keyArr[i];

    for (var j = 0; j < k.length; j++) {
      switch (k[j]) {
        case '[':
          ix_begin = j + 1;
          name_end = j;
          break;

        case ']':
          ix_end = j;
          break;

        case '+':
          if (ix_end === j - 1) a.add = true;
          break;

        case '?':
          name_end = j;
          if (ix_end === -1) o.nulls = true;
          break;

        case '{':
          filter_begin = j;
          break;

        case '}':
          filter_end = j + 1;
          break;

        default:
          if (ix_begin === -1) name_end = j + 1;
      }
    }

    if (name_end > 0) {
      o.name = k.substring(name_begin, name_end);
      keys[n++] = o;
    }

    if (ix_end > 0) {
      if (filter_end > 0) {
        a.filter = safeJsonParse(k.substring(filter_begin, filter_end));

        if (ix_begin === filter_begin) {
          if (ix_end !== filter_end) ix_begin = filter_end + 1;else ix_begin = filter_end;
        } else {
          ix_end = filter_begin - 1;
        }
      }

      a.ix = k.substring(ix_begin, ix_end);
      keys[n++] = a;
    }
  }

  return keys;
}

function safeJsonParse(str) {
  try {
    return JSON.parse(str);
  } catch (_unused) {
    console.log("Filter error with " + str);
  }
}

/**
 * A string of how to navigate through the incoming array is sent.
 * This is translated into an array of instructions for the recursive object
 * @param src
 * @param keystr
 */

function getKeyValue(src, keystr) {
  // Parse the source key string into an array/object format that is easy to recurse through
  var keys = parse(keystr); // Select the data from the source object or array

  var data = select(src, keys); // Return the data for further parsing

  return data;
}
/**
 * With a given source key array, select the corresponding value(s) in the source object/array.
 * If the value does not exist, return null
 * @param src
 * @param keys
 */

function select(src, keys) {
  if (keys === null) return null; // Get the object key or index that needs to be parsed

  if (typeof src === 'object' && src !== null) {
    var _keys$shift;

    var key = (_keys$shift = keys.shift()) !== null && _keys$shift !== void 0 ? _keys$shift : null;

    if (key !== null) {
      switch (key.type) {
        case 'object':
          return selectObj(src, key, keys);

        case 'array':
          return selectArr(src, key, keys);
      }
    }
  }

  return null;
}
/**
 * Loop through the array and select the key from each value in the array.  If nothing comes back, return null
 * @param src
 * @param key
 * @param keys
 */


function selectArr(src, key, keys) {
  var data = []; // The source is not an array even though we specify array.  Grab the subnode and add to an array.

  if (!Array.isArray(src)) {
    var d = null;

    if (keys !== null) {
      // Try to get the next value in the chain.  If possible, then add to an array
      if (keys.length) d = select(src, keys); // If we found something, return it as an array

      return d !== null ? [d] : null;
    }

    return null;
  } else {
    // check filter option
    if (key.filter) {
      src = filterArrayAccess(src, key.filter);
    } // Recursively loop through the array and grab the data


    for (var i = 0; i < src.length; i++) {
      if (keys !== null) {
        var value = src[i]; // Check to see if we are at a 'leaf' (no more keys to parse).  If so, return the data.  If not, recurse

        var _d = keys.length ? select(value, keys.slice()) : value; // If the data is populated, add it to the array.  Make sure to keep the same array index so that traversing multi-level arrays work


        if (_d !== null) data[i] = _d;
      }
    }
  } // Return the whole array if a specific index is not defined('') and there is data to return


  if (key.ix === '' && data.length) return data; // Return a specific node in the array if defined

  if (key.ix && typeof negativeArrayAccess(data, key.ix) !== 'undefined') return negativeArrayAccess(data, key.ix); // If we are not expecting an array, return the first node - kinda hack
  // if (typeof data[0] !== 'undefined' && key.name && data[0][key.name])
  //   return data[0][key.name];
  // Otherwise, return nothing

  return null;
}

function filterArrayAccess(arr, filter) {
  var query = new mingo.Query(filter);
  var cursor = query.find(arr);
  return cursor.all();
}
/**
 * Allows negative array indexes to count down from end of array
 * @param arr
 * @param ix
 */


function negativeArrayAccess(arr, ix) {
  var pix = parseInt(ix);
  return pix < 0 ? arr[arr.length + pix] : arr[pix];
}
/**
 * Traverse the given object for data using the given key arrays
 * @param src
 * @param key
 * @param keys
 */


function selectObj(src, key, keys) {
  // Make sure that there is data where we are looking
  if (src) {
    // && key.name
    // Match all keys in the object
    if (key.name === '*') return selectObjKeys(src, keys); // The key specifies an object.  However, the data structure is an array.  Grab the first node and continue

    if (Array.isArray(src)) {
      if (src.length && src[0]) {
        if (keys !== null) {
          var keyName = key.name;
          var srcFirst = src[0];
          var srcFirstName = undefined;
          if (typeof srcFirst === 'object' && !Array.isArray(srcFirst)) if (keyName in srcFirst) srcFirstName = srcFirst[keyName];

          if (keys.length) {
            return select(srcFirstName, keys);
          } else {
            return srcFirstName;
          }
        }
      } else {
        return null;
      }
    } else if (typeof src === 'object') {
      // The object has the given key
      var _name = key.name;

      if (_name in src) {
        var _value = src[_name];

        if (keys !== null) {
          // There is data to be obtained
          var data = keys.length ? select(_value, keys) : _value; // If there is data return it

          if (data !== null) return data;
        }
      }
    }
  } // Otherwise, return nothing


  return null;
}
/**
 * Loop through all the keys in the object and select the key from each key in the object.  If nothing comes back, return null
 * @param src
 * @param keys
 */


function selectObjKeys(src, keys) {
  var data = [];
  var n = 0; // Recursively loop through the object keys and grab the data
  //for (const k in src) {

  for (var v in Object.values(src)) {
    //for (const [k, v] in Object.entries(src)) {
    // Check to see if we are at a 'leaf' (no more keys to parse).  If so, return the data.  If not, recurse
    var d = keys.length ? select(v, keys.slice()) : v; //const d = keys.length ? select(v, keys.slice()) : v;
    // If the data is populated, add it to the array

    if (d !== null) data[n++] = d;
  } // Return the whole data array if there is data to return


  if (data.length) return data; // Otherwise, return nothing

  return null;
}

/**
 *
 *
 * @param {object} dest
 * @param {string} destination
 * @param {*} data
 * @param {*} [context={default:null,transform:null}]
 * @returns
 */

function setKeyValue(dest, destination, data, context) {
  if (context === void 0) {
    context = {};
  }

  // Keystr is undefined - call set_data in case there is a default or transformation to deal with
  if (typeof destination === 'undefined' || destination === null) return setData(dest, null, data, context); // Keystr is an array of values.  Loop through each and identify what format the individual values are

  if (Array.isArray(destination)) {
    for (var i = 0; i < destination.length; i++) {
      var keystrElm = destination[i];

      if (typeof keystrElm === 'string') {
        dest = setKeyValue(dest, keystrElm, data, context);
      } // The substring value is in array notation - recurse with the key from the array
      else if (Array.isArray(keystrElm)) {
          var k = keystrElm[0],
              t = keystrElm[1],
              d = keystrElm[2];
          if (typeof t !== 'undefined' && t !== null) context.transform = t;
          if (typeof d !== 'undefined' && d !== null) context["default"] = d;
          dest = setKeyValue(dest, k, data, context);
        } // The substring value is in object notation - dig further
        else if (typeof keystrElm === 'object' && keystrElm !== null) {
            if (typeof keystrElm.transform !== 'undefined') context.transform = keystrElm.transform;
            if (typeof keystrElm["default"] !== 'undefined') context["default"] = keystrElm["default"]; // If the substring value of the key is an array, parse the array.  If this is parsed in a recursion, it is confused with arrays containing multiple values

            if (Array.isArray(keystrElm.key)) {
              var _keystrElm$key = keystrElm.key,
                  _k = _keystrElm$key[0],
                  _t = _keystrElm$key[1],
                  _d = _keystrElm$key[2];
              if (typeof _t !== 'undefined') context.transform = _t;
              if (typeof _d !== 'undefined') context["default"] = _d;
              dest = setKeyValue(dest, _k, data, context);
            } // The substring value is regular object notation - recurse with the key of the substring
            else {
                var key = keystrElm.key;
                dest = setKeyValue(dest, key !== null && key !== void 0 ? key : null, data, context);
              }
          }
    }
  } // The value is in string notation - ready for update!
  else if (typeof destination === 'string') {
      dest = update(dest, data, parse(destination), context);
    } // The value is in object notation - dig a bit further
    else {
        var _destination$key2;

        if (typeof destination.transform !== 'undefined') context.transform = destination.transform;
        if (typeof destination["default"] !== 'undefined') context["default"] = destination["default"]; // If the value of the key is an array, parse the array.  If this is parsed in a recursion, it is confused with arrays containing multiple values

        if (Array.isArray(destination.key)) {
          var _destination$key = destination.key,
              _k2 = _destination$key[0],
              _t2 = _destination$key[1],
              _d2 = _destination$key[2];
          if (typeof _t2 !== 'undefined' && _t2 !== null) context.transform = _t2;
          if (typeof _d2 !== 'undefined' && _d2 !== null) context["default"] = _d2;
          dest = setKeyValue(dest, _k2, data, context);
        } // The value is in regular object notation.  Recurse with the object key
        else dest = setKeyValue(dest, (_destination$key2 = destination.key) !== null && _destination$key2 !== void 0 ? _destination$key2 : null, data, context);
      }

  return dest;
} // Set the given data into the given destination object

function setData(dest, key, data, context) {
  // | AnyArray | null {
  // See if data is null and there is a default
  if (typeof context["default"] !== 'undefined' && (data === null || typeof data === 'undefined')) {
    // There is a default function, call the function to set the default
    if (typeof context["default"] === 'function') {
      var _context$default;

      dest = dest || {};
      data = (_context$default = context["default"](context.src, context.srckey, dest, context.destkey)) !== null && _context$default !== void 0 ? _context$default : null;
    } // The default is a specific value
    else data = context["default"];
  } // If there is a transformation function, call the function.


  if (typeof context.transform === 'function') {
    var _context$transform;

    dest = dest || {};
    data = (_context$transform = context.transform(data, context.src, dest, context.srckey, context.destkey)) !== null && _context$transform !== void 0 ? _context$transform : null;
  } // Set the object to the data if it is not undefined
  // if (typeof data !== 'undefined' && key) {


  if (data != null && key) {
    if (key.type === 'object') {
      // Set the data if the data is not null, or if the 'allow nulls' key is set, or if there is a default (in the case of default=null, make sure to write this out)
      if (data != null || key.nulls || typeof context["default"] !== 'undefined' && context["default"] === null) {
        if (dest === null || typeof dest === 'undefined') dest = {};
        dest = dest || {};
        var targetName = key.name;
        if (!Array.isArray(dest)) dest[targetName] = data;
      }
    }
  } // Return the dest variable back to the caller.


  return dest;
}
/**
 * if the data is an array, walk down the obj path and build until there is an array key
 * @param dest
 * @param value
 * @param keys
 * @param context
 */


function update(dest, value, keys, context) {
  if (keys) {
    // Get the object key and index that needs to be parsed
    var key = keys.shift();

    if ( //   typeof dest === 'object' &&
    //   dest !== null &&
    key != null // && typeof key !== 'undefined'
    ) {
        switch (key.type) {
          case 'object':
            //if (!Array.isArray(dest))
            return updateObj(dest, key, value, keys, context);
          //break;

          case 'array':
            if (Array.isArray(dest)) {
              return updateArr(dest, key, value, keys, context);
            } else {
              return updateArr([], key, value, keys, context);
            }

          //break;
        }
      }
  }

  return setData(dest, null, value, context);
}
/**
 * Update the destination object.key with the data
 * @param dest
 * @param key
 * @param value
 * @param keys
 * @param context
 */
// Update the destination object.key with the data


function updateObj(dest, key, data, keys, context) {
  // There are further instructions remaining - we will need to recurse
  if (keys.length) {
    // There is a pre-existing destination object.  Recurse through to the object key
    if (dest != null && // typeof dest !== 'undefined' &&
    !Array.isArray(dest) // && key.name in dest
    ) {
        var givenDest = null;

        if (key.name in dest) {
          var found = dest[key.name];
          if (typeof found !== 'object') givenDest = null;else givenDest = found;
        }

        var o = update(givenDest, data, keys, context); // ??

        if (o != null) {
          // && typeof o !== 'undefined') {
          dest[key.name] = o;
        } // ToDo: Change
        // else{
        //   console.log(dest[key.name])
        //   // delete dest[key.name] // ??
        //   return dest;
        // }
        // const o =
        //   typeof givenDest === 'object'
        //     ? update(givenDest, data, keys, context)
        //     : null;
        // if (o !== null && typeof o !== 'undefined') dest[key.name] = o;

      } // There is no pre-existing object.  Check to see if data exists before creating a new object
    else {
        // Check to see if there is a value before creating an object to store it
        var _o = update(null, data, keys, context);

        if (_o !== null) {
          dest = {};
          dest[key.name] = _o;
        }
      }
  } // This is a leaf.  Set data into the dest
  else dest = setData(dest, key, data, context);

  return dest;
}
/**
 * Update the dest[] array with the data on each index
 * @param dest
 * @param key
 * @param value
 * @param keys
 * @param context
 */


function updateArr(dest, key, data, keys, context) {
  // The 'add' instruction is set.  This means to take the data and add it onto a new array node
  if (key.add) {
    if (data != null) {
      //&& typeof data !== 'undefined') {
      dest = dest || [];
      dest.push(applyTransform(data, dest, context)); // dest = dest.concat(data)
    }

    return dest;
  } // Just update a single array node


  if (key.ix !== '') {
    return updateArrIx(dest, parseInt(key.ix), applyTransform(data, dest, context), keys, context);
  } // If the data is in an array format then make sure that there is a dest index for each data index


  if (Array.isArray(data)) {
    if (dest === null || typeof dest === 'undefined') dest = []; // Loop through each index in the data array and update the destination object with the data

    dest = data.reduce(function (dest, d, i) {
      // If the instruction is to update all array indices ('') or the current index, update the child data element.  Otherwise, don't bother
      if (key.ix === '' || parseInt(key.ix) === i) {
        return updateArrIx(dest, i, applyTransform(d, dest, context), keys.slice(), context);
      } else {
        return [];
      }
    }, dest);
    return dest;
  } // Set the specific array index with the data
  else return updateArrIx(dest, 0, data, keys, context);
}

function applyTransform(data, dest, context) {
  if (typeof context.transform === 'function') return context.transform(data, context.src, dest, context.srckey, context.destkey);else return data;
}

function updateArrIx(dest, ix, data, keys, context) {
  var o; // ToDo: Change

  if (dest != null) {
    var _dest$ix;

    //&& typeof dest !== 'undefined') {
    var givenDest = (_dest$ix = dest[ix]) !== null && _dest$ix !== void 0 ? _dest$ix : null;

    if (keys.length && (typeof givenDest === 'object' || givenDest === null)) {
      o = update(givenDest, data, keys, context);
    } else {
      o = data;
    }
  } else {
    o = keys.length ? update(null, data, keys, context) : data;
  } // Only update (and create if needed) dest if there is data to be saved
  // ToDo: Change


  if (o != null) {
    dest = dest || [];
    dest[ix] = o;
  }

  return dest;
}

function ObjectMapper(src, dest, map) {
  var destObj, mapObj; // There are two different constructors - move around properties if needed
  // e.g (ObjectMapper(from,map))

  if (typeof map === 'undefined') {
    mapObj = dest;
    destObj = null;
  } else {
    mapObj = map;
    destObj = dest;
  } // let idx = 0
  // Loop through the map to process individual mapping instructions


  for (var _i = 0, _Object$entries = Object.entries(mapObj); _i < _Object$entries.length; _i++) {
    var _Object$entries$_i = _Object$entries[_i],
        srckey = _Object$entries$_i[0],
        destination = _Object$entries$_i[1];
    // console.log(´idx:{idx}´)
    // idx++
    // Extract the data from the source object or array
    var data = getKeyValue(src, srckey);

    if (data instanceof Array) {
      if (data.length > 0) {
        var _ref;

        //ts-ignore
        data = (_ref = []).concat.apply(_ref, data);
      }
    } //const data = [...getKeyValue(src, srckey)];
    // Build an object with all of these parameters in case custom transform or default functions need them to derive their values


    var context = {
      src: src,
      srckey: srckey,
      destkey: destination
    }; // Set the data into the destination object or array format

    destObj = setKeyValue(destObj, destination, data, context);
  }

  return destObj;
}

// export const sum = (a: number, b: number) => {

exports.ObjectMapper = ObjectMapper;
exports.default = ObjectMapper;
exports.getKeyValue = getKeyValue;
exports.om = ObjectMapper;
exports.parse = parse;
exports.setKeyValue = setKeyValue;
exports.split = split;
//# sourceMappingURL=calibrate-json-mapper.cjs.development.js.map
