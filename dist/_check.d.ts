declare const dataArr: string[];
declare const dataObj: {
    d: number;
    e: number[];
    f: {
        g: number;
    };
};
declare let map_001: string[];
declare let map_002: Map<string, number | number[] | {
    g: number;
}>;
