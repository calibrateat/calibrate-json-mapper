import { ObjectMapper, getKeyValue, setKeyValue, parse, split } from './lib';
export { ObjectMapper, ObjectMapper as om, getKeyValue, setKeyValue, parse, split };
export default ObjectMapper;
